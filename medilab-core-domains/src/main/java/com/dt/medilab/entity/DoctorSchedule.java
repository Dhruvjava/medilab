package com.dt.medilab.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name = "DoctorSchedule")
@Data
public class DoctorSchedule implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int schdId;
	
	@OneToOne
	private MedilabUser doctor;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL,mappedBy ="doctorSchedule")
	private Set<DoctorAvailibility> doctorAvailibilities;

	private String startTime;

	private String endTime;

	private String message;

	private String scheduleStatus;
	
	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = false)
	private String lastModifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreateTime")
	private Date createTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LastModifiedTime")
	private Date lastModifiedTime;

}
