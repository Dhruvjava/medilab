package com.dt.medilab.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "MedilabAuthentication")
public class MedilabAuthentication implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3246040849514897597L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String username;
	
	private String password;
	
	

}
