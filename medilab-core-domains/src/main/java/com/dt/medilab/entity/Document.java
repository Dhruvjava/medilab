package com.dt.medilab.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "Document")
@Data
public class Document implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2701598557894960982L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int documentId;
	
	private String title;

	private String discription;

	private String documentPath;

	private String docKey;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = false)
	private String lastModifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreateTime")
	private Date createTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LastModifiedTime")
	private Date lastModifiedTime;
	
	@Transient
	private String loggedInUser;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "statusID")
	private DocumentStatus status;
	
	
	
	

}
