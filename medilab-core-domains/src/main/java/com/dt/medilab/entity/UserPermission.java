/**
 * 
 */
package com.dt.medilab.entity;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.sql.DataSourceDefinition;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Dhruv Kumar
 *
 */

@Entity
@Table(name = "UserPermission")
@Data
public class UserPermission extends AuditInfo implements Serializable {


	private static final long serialVersionUID = 524736261862378419L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(nullable=false, unique = true, updatable = true)
	private String name;
	
	

}
