package com.dt.medilab.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name = "AvailaleDays")
@Data
public class AvailaleTime implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7440935194971874667L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String Day;
	
	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = false)
	private String lastModifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreateTime")
	private Date createTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LastModifiedTime")
	private Date lastModifiedTime;

	
	
}
