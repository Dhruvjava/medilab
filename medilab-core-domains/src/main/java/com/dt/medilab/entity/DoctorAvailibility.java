package com.dt.medilab.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "DoctorAvailibility")
@Data
public class DoctorAvailibility implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "schdId")
	private DoctorSchedule doctorSchedule;
	
	@ManyToOne
	@JoinColumn(name = "availaleDays")
	private AvailaleDays availaleDays;
	
	@ManyToOne
	@JoinColumn(name = "availaleTime")
	private AvailaleTime availaleTime;
}
