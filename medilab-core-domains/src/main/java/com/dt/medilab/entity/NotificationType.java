/**
 * 
 */
package com.dt.medilab.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Dhruv Kumar
 *
 */

@Entity
@Table(name = "NotificationType")
@Data
public class NotificationType implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 4685274163046992354L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int typeId;
	
	private String notificationType;

}
