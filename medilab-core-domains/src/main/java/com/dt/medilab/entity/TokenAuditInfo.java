package com.dt.medilab.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "TokenAuditInfo")
public class TokenAuditInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8197513126721311946L;
	
	/*
	 * 
	 * Audit Info
	 * 
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String createdy;
	private Date createdDate;
	
	private String modifiedBy;
	
	private Date modifiedDate;

	private String authnStore;

	private Date token_Refreshed_Time;

	private String older_Access_Token;

	private String new_Access_Token;
	
	private String username;
	
	@OneToOne
	private MedilabAuthnToken authToken;

}
