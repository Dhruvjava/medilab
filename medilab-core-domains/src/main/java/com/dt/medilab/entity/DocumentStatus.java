package com.dt.medilab.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "DocumentStatus")
@Data
public class DocumentStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2025939613989717675L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "statusID")
	private int id;
	
	@Column(name = "status")
	private String statusName;

}
