package com.dt.medilab.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
public class AuditInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8197513126721311946L;
	
	/*
	 * 
	 * Audit Info
	 * 
	 */
	
	private String createdy;
	private Date createdDate;
	
	private String modifiedBy;
	
	private Date modifiedDate;

}
