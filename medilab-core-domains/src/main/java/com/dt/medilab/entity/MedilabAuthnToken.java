package com.dt.medilab.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "MedilabAuthentication")
public class MedilabAuthnToken extends AuditInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6845737367722935187L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String access_Token;
	
	private String id_Token;
	
	private String refresh_Token;

	private String token_Type;

	private String expiry;
	
	
}
