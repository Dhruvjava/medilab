package com.dt.medilab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedilabCoreDomainsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedilabCoreDomainsApplication.class, args);
	}

}
